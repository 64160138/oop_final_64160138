/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.punyawat.oopfin;

/**
 *
 * @author punya
 */
public class Register{
    private String userName,passWord,re_PassWord,Email;
    public Register(String userName,String passWord,String re_PassWord,String Email){
        this.userName = userName;
        this.passWord = passWord;
        this.re_PassWord = re_PassWord;
        this.Email = Email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getRe_PassWord() {
        return re_PassWord;
    }

    public void setRe_PassWord(String re_PassWord) {
        this.re_PassWord = re_PassWord;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    @Override
    public String toString() {
        return "Register{" + "userName=" + userName + ", passWord=" + passWord + ", re_PassWord=" + re_PassWord + ", Email=" + Email + '}';
    }
    
    
}
